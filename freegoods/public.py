from flask import Blueprint, render_template, request
from flask_login import current_user

from freegoods.models import Products, Category

public = Blueprint("public", __name__)


@public.route("/")
def index():
    search_category = request.args.get("category", None, type=str)
    search_item = request.args.get("search-item", None, type=str)

    all_products = Products.query

    if search_category:
        category = Category.query.filter_by(name=search_category.capitalize()).first()
        all_products = all_products.filter_by(category_id=category.id)

    if search_item:
        all_products = all_products.filter(Products.name.like(f"%{search_item}%"))

    all_products = all_products.order_by(Products.id.desc()).all()

    liked_product_ids = []
    if current_user.is_authenticated:
        liked_product_ids = [like.product_id for like in current_user.likes]

    return render_template("index.html",
                           catalog_title="All Products" if not search_category else f"All {search_category.capitalize()} Products"
                           , all_products=all_products
                           , search_item=search_item
                           , liked_product_ids=liked_product_ids)
