from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import current_user, login_user, logout_user

from freegoods.models import User
from freegoods.extns import db

auth = Blueprint('auth', __name__)


@auth.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('public.index'))

    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')

        user = User.query.filter_by(username=username).first()
        if not user:
            flash("User does not exist", category="danger")
            return redirect(url_for('auth.login'))

        if user.password != password:
            flash("Incorrect password", category="danger")
            return redirect(url_for('auth.login'))

        login_user(user)
        if current_user.is_admin:
            return redirect(url_for('admin.index'))
        return redirect(url_for('public.index'))

    return render_template("login.html")


@auth.route("/register", methods=["GET", "POST"])
def register():

    if current_user.is_authenticated:
        return redirect(url_for('public.index'))

    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        confirm_password = request.form.get('confirm_password')
        is_admin = request.form.get('is_admin')

        user = User.query.filter_by(username=username).first()
        if user:
            flash("Username already taken", category="danger")
            return redirect(url_for('auth.register'))

        if confirm_password != password:
            flash("Password Didn't match", category="danger")
            return redirect(url_for('auth.register'))

        try:
            new_user = User(username=username, password=password, is_admin=True if is_admin else False)
            db.session.add(new_user)
            db.session.commit()
        except:
            db.session.rollback()
            flash("An error occurred while creating your account. Please try again.", category="danger")
            return redirect(url_for('auth.register'))

        flash("Account Created Successfully, Please Login", category="success")
        return redirect(url_for('auth.login'))

    return render_template("register.html")


@auth.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("auth.login"))
