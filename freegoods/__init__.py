from flask import Flask

from freegoods.auth import auth
from freegoods.public import public
from freegoods.admin import admin
from freegoods.extns import db, login_manager
from freegoods.user import user
from freegoods.models import User, Category


class Configs:
    SECRET_KEY = "this-is-secretkey"
    SQLALCHEMY_DATABASE_URI = "sqlite:///database.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    APP_NAME = "Free Goods"


def create_app():
    app = Flask(__name__)
    app.config.from_object(Configs)

    app.register_blueprint(auth)
    app.register_blueprint(public)
    app.register_blueprint(user)
    app.register_blueprint(admin)

    login_manager.init_app(app)

    db.init_app(app)
    with app.app_context():
        import freegoods.models
        db.create_all()

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    app.add_template_global(name="global_all_categories", f=lambda: Category.query.all())

    return app
