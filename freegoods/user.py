import uuid

from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_required, current_user
from sqlalchemy import or_

from freegoods.extns import db
from freegoods.models import Products, Likes, Chat, User, Review

user = Blueprint("user", __name__, url_prefix="/user")


@user.before_request
@login_required
def before_request():
    pass


@user.route("/profile")
def profile():

    return render_template("profile.html")


@user.get("/likes")
def likes():
    liked_product_ids = [like.product_id for like in current_user.likes]
    liked_products = Products.query.filter(Products.id.in_(liked_product_ids)).all()
    return render_template("likes.html"
                           , liked_products=liked_products
                           , liked_product_ids=liked_product_ids)


@user.post("/add_listing")
def add_listing():
    title = request.form.get("title")
    image = request.files.get("image")
    description = request.form.get("description")
    category = request.form.get("category")

    file_name = str(uuid.uuid4()) + "." + image.filename.split(".")[-1]
    image.save(f"freegoods/static/images/{file_name}")

    product = Products(
        name=title,
        description=description,
        image_path=file_name,
        category_id=category,
        added_by=current_user
    )
    db.session.add(product)
    db.session.commit()

    flash("Listing added successfully", "success")

    return redirect(url_for("user.profile"))


@user.get("/like_product/<int:product_id>")
def like_product(product_id):
    product = Products.query.filter_by(id=product_id).first()
    new_like = Likes(user_id=current_user.id, product_id=product.id)
    current_user.likes.append(new_like)

    db.session.commit()
    flash("Liked successfully", "success")

    return redirect(request.referrer or url_for("public.index"))


@user.get("/unlike_product/<int:product_id>")
def unlike_product(product_id):
    product = Products.query.filter_by(id=product_id).first()
    like = Likes.query.filter_by(user_id=current_user.id, product_id=product.id).first()
    like.remove_like()

    db.session.commit()
    flash("Unliked successfully", "success")

    return redirect(request.referrer or url_for("public.index"))


@user.get("/chat/<username>")
def chat(username):
    if not username == "all":
        chat_with = User.query.filter_by(username=username).first()
        if not chat_with:
            flash("User does not exist", "danger")
            return redirect(url_for("public.index"))

        chats = Chat.query.filter_by(sender_id=current_user.id, receiver_id=chat_with.id).first() or Chat.query.filter_by(
            sender_id=chat_with.id, receiver_id=current_user.id).first()

        if chats:
            user_convo = Chat.query.filter_by(conversation_id=chats.conversation_id).all()
        else:
            user_convo = None
    else:
        chat_with = None
        user_convo = None

    inbox_chat = (
        db.session.query(Chat)
        .filter(or_(Chat.sender_id == current_user.id, Chat.receiver_id == current_user.id))
        .group_by(Chat.conversation_id)
        .all()
    )

    inbox_user_list = list()
    for chat in inbox_chat:
        if chat.receiver_id == current_user.id:
            inbox_user_list.append(chat.sender.username)
        else:
            inbox_user_list.append(chat.receiver.username)

    return render_template("chat.html"
                           , chat_with=chat_with
                           , user_convo=user_convo
                           , inbox_user_list=inbox_user_list)


@user.post("/chat")
def chat_post():
    message = request.form.get("message")
    receiver_id = request.form.get("receiver_id")

    chat_exist = Chat.query.filter_by(sender_id=current_user.id,
                                      receiver_id=receiver_id).first() or Chat.query.filter_by(sender_id=receiver_id,
                                                                                               receiver_id=current_user.id).first()

    if not chat_exist:
        convo_id = str(uuid.uuid4())
    else:
        convo_id = chat_exist.conversation_id

    new_message = Chat(
        sender_id=current_user.id,
        receiver_id=receiver_id,
        message=message,
        conversation_id=convo_id
    )
    db.session.add(new_message)
    db.session.commit()

    return {"success": True}, 200


@user.route("/leave_review/<username>", methods=["GET", "POST"])
def leave_review(username):

    user = User.query.filter_by(username=username).first()
    if not user:
        flash("User does not exist", "danger")
        return redirect(url_for("public.index"))

    if request.method == 'POST':
        review = request.form.get("review")

        new_review = Review(
            user_id=user.id,
            reviewer_id=current_user.id,
            comment=review
        )
        db.session.add(new_review)
        db.session.commit()

        flash("Review added successfully", "success")
        return redirect(url_for("public.index"))

    return render_template("leave_review.html"
                           , user=user)


@user.get("/review")
def reviews():
    return render_template("my_reviews.html")


@user.get("/remove_listing/<int:product_id>")
def remove_listing(product_id):
    product = Products.query.filter_by(id=product_id).first()
    db.session.delete(product)
    db.session.commit()

    flash("Listing removed successfully", "success")

    return redirect(request.referrer or url_for("user.profile"))