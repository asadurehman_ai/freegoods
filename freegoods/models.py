from datetime import datetime

from flask_login import UserMixin
from sqlalchemy import Column, Integer, String, Text, Boolean

from freegoods.extns import db


class Likes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    product_id = db.Column(db.Integer, db.ForeignKey("products.id"))

    def remove_like(self):
        db.session.delete(self)
        db.session.commit()


class User(db.Model, UserMixin):
    id = Column(Integer, primary_key=True)
    username = Column(String(80), unique=True)
    password = Column(String(80))
    is_admin = Column(Boolean, default=False)

    likes = db.relationship("Likes", backref="user")


class Category(db.Model):
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(80), unique=True)

    products = db.relationship("Products", backref="category")


class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(String(255))
    description = db.Column(Text)
    image_path = db.Column(String(255))
    added_on = db.Column(db.DateTime, default=datetime.now())

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    category_id = db.Column(db.Integer, db.ForeignKey("category.id"))

    added_by = db.relationship("User", backref="products")
    likes = db.relationship("Likes", backref="product")


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    receiver_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    message = db.Column(db.Text)
    sent_on = db.Column(db.DateTime, default=datetime.now())

    conversation_id = db.Column(db.String)

    sender = db.relationship("User", foreign_keys=[sender_id])
    receiver = db.relationship("User", foreign_keys=[receiver_id])


class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    reviewer_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    comment = db.Column(db.Text)

    user = db.relationship("User", foreign_keys=[user_id], backref="reviews")
    reviewer = db.relationship("User", foreign_keys=[reviewer_id], backref="reviewers")
