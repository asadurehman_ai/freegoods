from flask import Blueprint, render_template

from freegoods.models import Products

admin = Blueprint("admin", __name__, url_prefix="/admin")


@admin.route("/")
def index():
    all_products = Products.query.order_by(Products.id.desc()).all()

    return render_template("admin_dashboard.html"
                           , all_products=all_products)
